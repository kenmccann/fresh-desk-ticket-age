import requests
import dateutil.parser
from datetime import datetime, timezone, timedelta

authorization_code = ""

headers = {
    'Authorization': "Basic " + authorization_code,
    'Cache-Control': "no-cache"
}

pages = []
tickets = []
morepages = True
page_count = 0
query_results = 0
search_start = '2015-01-01'
items_per_page = 100
r = None
unresolved_status = [2, 3, 6, 8, 9, 10, 11, 12, 13, 14]
open_tickets = 0

print("Gathering tickets...")
print("")

while morepages:
    page_count += 1
    if page_count > 15:
        morepages = False
        print("Maximum pages reached.")
        break

    #    params = {'query': query_string, 'page': str(page_count)}
    #    r = requests.get('https://aquasecurity.freshdesk.com/api/v2/search/tickets', headers=headers, params=params)

    params = {'updated_since': search_start, 'per_page': str(items_per_page), 'page': str(page_count)}
    r = requests.get('https://aquasecurity.freshdesk.com/api/v2/tickets', headers=headers, params=params)

    json_data = r.json()
    if json_data == [] or r.status_code != 200:
        if r.status_code != 200:
            print('Failure: ' + r.reason)
        morepages = False
        break

    pages.append(json_data)

for page in pages:
    for ticket in page:
        #print(ticket['created_at'] + " Status: " + str(ticket['status']))
        tickets.append(ticket)

fifteen_day_bucket = 0
thirty_day_bucket = 0
sixty_day_bucket = 0
sixtyplus_day_bucket = 0

for issue in tickets:
    if issue['status'] in unresolved_status:
        open_tickets += 1
        ticket_opened = dateutil.parser.parse(issue['created_at'])
        if ticket_opened > datetime.now(timezone.utc) - timedelta(1):
            #print(str(issue['id']) + " was opened today.")
            fifteen_day_bucket += 1
        elif ticket_opened > datetime.now(timezone.utc) - timedelta(15):
            #print(str(issue['id']) + " was opened less than 5 days ago.")
            fifteen_day_bucket += 1
        elif ticket_opened > datetime.now(timezone.utc) - timedelta(30):
            #print(str(issue['id']) + " was opened less than 30 days ago.")
            thirty_day_bucket += 1
        elif ticket_opened > datetime.now(timezone.utc) - timedelta(60):
            #print(str(issue['id']) + " was opened less than 60 days ago.")
            sixty_day_bucket += 1
        else:
            #print(str(issue['id']) + " was opened more than 60 days ago.")
            sixtyplus_day_bucket += 1

print("Total open tickets " + str(open_tickets))
print("-----------------------------------------------------")
print("Issues 0-15 days old: " + str(fifteen_day_bucket) + " (%.0f percent)" % (fifteen_day_bucket / open_tickets * 100))
print("Issues 15-30 days old: " + str(thirty_day_bucket) + " (%.0f percent)" % (thirty_day_bucket / open_tickets * 100))
print("Issues 30-60 days old: " + str(sixty_day_bucket) + " (%.0f percent)" % (sixty_day_bucket / open_tickets * 100))
print("Issues 60+ days old: " + str(sixtyplus_day_bucket) + " (%.0f percent)" % (sixtyplus_day_bucket / open_tickets * 100))
print("-----------------------------------------------------")
