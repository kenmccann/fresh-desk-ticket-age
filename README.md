# Fresh Desk Ticket Age #

Python script for scraping age of currently open tickets in Freshdesk using the Freshdesk REST API.


### How do I get set up? ###

* Install python3 (https://www.python.org/downloads/)
* Install dependent libraries with pip:
   `pip install requests python-dateutil`
* **Note**: You must obtain an authorized key and enter it on line 5 (authorization_code)
* You can also base64 encode your username:password 


### Running ###

To run the script, simply call it from the command line with no parameters.

`python3 freshDeskAge.py`

### Questions? ###

* Contact: ken@aquasec.com
